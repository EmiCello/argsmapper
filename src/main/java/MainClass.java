import java.util.Map;
import java.util.TreeMap;

public class MainClass {

    public static void main(String[] args) {


        String[] input = {"S", "mama i tata", "-p", "8080", "-d", "/usr/logs","I", "123", "B", "B", };


        Map<String, Configuration> map = new TreeMap<>();
        map.put("I", Configuration.INTEGER);
        map.put("S", Configuration.STRING);
        map.put("B", Configuration.BOOLEAN);


        Map<String, Object> parse = new ArgsMapper(map).parse(input);

    }
}
