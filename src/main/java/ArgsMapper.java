import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Stream;

public class ArgsMapper {

    Map<String, Configuration> map;


    public ArgsMapper(Map<String, Configuration> map) {
        this.map = map;
    }



    public Map<String, Object> parse(String[] args){

        Map<String, Object> map1 = new TreeMap<>();


        for(int i = 0; i < args.length; i = i +2){

            if(args[i].contains("-")){
                StringBuilder s = new StringBuilder(args[i]);
                args[i] = s.delete(0,1).toString();
            }

            if(!map.keySet().contains(args[i])){
                System.out.println("nie zawiera tego klucza");
            }else {

                switch(map.get(args[i])){
                    case INTEGER :
                        try{
                            int a = Integer.parseInt(args[i + 1]);
                            map1.put(args[i], Integer.parseInt(args[i + 1]));
                            break;
                        }catch(NumberFormatException e){
                            System.out.println("This is not a number!");
                        }

                    case STRING :
                        map1.put(args[i], args[i + 1]);
                        break;
                    case BOOLEAN :
                        map1.put(args[i], true);
                        i -= 1;
                        break;
                    default :
                }

            }

        }

        System.out.println(map1.keySet() + " " + map1.values());

        return map1;
    }

}


