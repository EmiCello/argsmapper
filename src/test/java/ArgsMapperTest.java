import org.junit.Test;

import java.util.Map;
import java.util.TreeMap;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class ArgsMapperTest {

    @Test
    public void doesThrowNonExistentKey(){
        Map<String, Configuration> map2 = new TreeMap<>();
        map2.put("I", Configuration.INTEGER);
        map2.put("S", Configuration.STRING);

        ArgsMapper argsMapper = new ArgsMapper(map2);

        String[] input = {"I", "8080", "S", "/usr/logs"};

        Map<String, Object> mapHelper = argsMapper.parse(input);

        assertFalse(mapHelper.keySet().contains("-p"));
        assertEquals("/usr/logs", mapHelper.get("S"));
    }

    @Test
    public void isTheSame(){

        Map<String, Configuration> map2 = new TreeMap<>();
        map2.put("I", Configuration.INTEGER);
        map2.put("S", Configuration.STRING);

        ArgsMapper argsMapper = new ArgsMapper(map2);

        String[] input = {"I", "8080", "S", "/usr/logs"};

        Map<String, Object> mapHelper = argsMapper.parse(input);

        assertEquals(8080, mapHelper.get("I"));
        assertEquals("/usr/logs", mapHelper.get("S"));
    }


}
